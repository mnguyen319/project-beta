# CarCar

Team:

* Richard - Sales Microservice
* Person 2 - Which microservice?

## Design
We divdided the whole project into three bonded contexts: Inventory, services, and sales. From the main page, we created some links on Nav bar to connect to the specific page of each microservice and detail inventory elements. We created cards display on the main page to show up cars and pictures.
## Service microservice

Explain your models and integration with the inventory
microservice, here.

## Sales microservice

For the sales microservice, I created four models: automobile value object model, sales person model, potential customer model, and auto sales model.
The automobile value object model is used to store the automobile data imported from the inventory microservice, since I need to use some details about the vechile automobile while creating the sales list.
The sales person model is used to store the sales person data, create, update, or delete the data. 
The potential customer model is used to store the customer data, create, update, or delete the data.
The auto sales model is used to store the sales list data, which include the information about sales person, potential customer, and automobile. It has a many-to-one relation to the sales person model, the potential customer model, and the automobile model. (There might be a special scenario that one specific vehicle was sold from the dealer, and after couple days, the car owner sell it back to the dealer, and we have to add it back to the inventory. Again, it was sell to another person, at the same price. Then on the sales list, there will be two items with same automobile information. In this case, the automobile information cannot be unique, and once we delete one item from the list, we dont want the other item been affected.)
After that, I set up a poller to connect the automobile VO model with the auto mobile model from inventory. In this case, I can poll data from there.
I used these data to create the sales list on the front-end. Also created couple forms page to create new data and send it back to the api data base.