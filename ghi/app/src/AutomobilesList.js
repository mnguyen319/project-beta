import React from "react";
import { Link } from "react-router-dom";


function AutomobilesList(props) {
    return(
        <table className="table table-striped table-hover">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Color</th>
              <th>Year</th>
              <th>Model</th>
              <th>Manufacturer</th>
            </tr>
          </thead>
          <tbody>
            {props.autos.map(auto => {
              return (
                <tr key={auto.id}>
                  <td>{ auto.vin }</td>
                  <td>{ auto.color }</td>
                  <td>{ auto.year }</td>
                  <td>{auto.model.name}</td>
                  <td>{auto.model.manufacturer.name}</td>
                </tr>
              );
            })}
            <tr>
              <td>
                <Link to='/automobiles/new'>Add automobile</Link>
              </td>
              <td>
                <Link to='/automobiles/new'>Delete automobile</Link>
              </td>
            </tr>
          </tbody>
        </table>
    )
};

export default AutomobilesList;