import React from 'react';

class DeleteSalesPersonForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sales_persons: []
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleSalesPersonSelection = this.handleSalesPersonSelection.bind(this);
  }

  async componentDidMount() {
    const url = 'http://localhost:8090/api/sales_person/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ sales_persons: data.sales_person });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();

    const salesPersonUrl = `http://localhost:8090/api/sales_person/${this.state.sales_person}`;
    const fetchConfig = {
      method: "delete",
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(salesPersonUrl, fetchConfig);
    if (response.ok) {
      const newSalesPerson = await response.json();
      console.log(newSalesPerson)

      // Remove hat from state without making new API call
      let remainingSalesPersons = []
      for (let i = 0; i < this.state.sales_persons.length; i++) {
        let currentSalesPerson = this.state.sales_persons[i]
        if (parseInt(this.state.sales_person) !== currentSalesPerson.id) {
            remainingSalesPersons.push(currentSalesPerson)
        }
      }

      this.setState({
        sales_person: '',
        sales_persons: remainingSalesPersons
      })
    }
  }

  handleSalesPersonSelection(event) {
    const value = event.target.value;
    this.setState({ sales_person: value });
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Select Sales Person To Remove</h1>
            <form onSubmit={this.handleSubmit} id="delete-sales-person-form">
              <div className="mb-3">
                <select onChange={this.handleSalesPersonSelection} value={this.state.sales_person} required name="sales_person" id="sales_person" className="form-select">
                  <option value="">Choose a sales person</option>
                  {this.state.sales_persons.map(sales_person => {
                    return (
                      <option key={sales_person.id} value={sales_person.id}>{sales_person.name}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Remove</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default DeleteSalesPersonForm;