import React from 'react';

class VheicleModelForm extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            picture_url: '',
            manufacturer_id:'',
            manufacturers: [],
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handlePictureChange = this.handlePictureChange.bind(this);
        this.handleManufacturersChange = this.handleManufacturersChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    };

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.manufacturers;
        const modelUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(modelUrl, fetchConfig);
        if (response.ok) {
            const newmodel = await response.json();
            console.log(newmodel);
            const cleared = {
                name: '',
                picture_url: '',
                manufacturer: '',
              };
              this.setState(cleared);
        }
    };

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value});
    };

    handlePictureChange(event) {
        const value = event.target.value;
        this.setState({picture_url: value});
    };

    handleManufacturersChange(event) {
        const value = event.target.value;
        this.setState({manufacturer_id: value});
    };

    async componentDidMount() {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({manufacturers: data.manufacturers});
            };
        };


    render() {
        return(
            <div className="my-5 container">
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1>New vehicle model</h1>
                            <form onSubmit={this.handleSubmit} id="create-model-form">
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text"  name="name" id="name" className="form-control"/>
                                    <label htmlFor="name">Name</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={this.handlePictureChange} value={this.state.picture_url} placeholder="Picture Url" required type="text" name="picture_url" id="picture_url" className="form-control"/>
                                    <label htmlFor="picture_url">Picture Url</label>
                                </div>
                                <div className="mb-3">
                                    <select onChange={this.handleManufacturersChange} value={this.state.manufacturer_id} required name="manufacturers" id="manufacturers" className="form-select">
                                    <option value="">Choose a manufacturer</option>
                                    {this.state.manufacturers.map(manufacturer => {
                                        return (
                                            <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                                        );
                                    })}
                                    </select>
                                </div>
                                <button className="btn btn-primary">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    };

};
export default VheicleModelForm;