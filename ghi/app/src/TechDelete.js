import React from 'react';

class TechnicianDelete extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      technician: "",
      technicians: []
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleTechnicianSelection = this.handleTechnicianSelection.bind(this);
  }

  async componentDidMount() {
    const url = 'http://localhost:8080/api/technicians/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ technicians: data.technicians });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    // delete data.manufacturers

    const locationUrl = `http://localhost:8080/api/technicians/${this.state.technician}/`;
    const fetchConfig = {
      method: "delete",
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newTechnician = await response.json();
      console.log(newTechnician)

      let remainingTechnicians = []
      for (let i = 0; i < this.state.technicians.length; i++) {
        let currentTechnician = this.state.technicians[i]
        if (parseInt(this.state.technician) !== currentTechnician.id) {
          remainingTechnicians.push(currentTechnician)
        }
      }

      this.setState({
        technician: '',
        technicians: remainingTechnicians
      })
    }
  }

  handleTechnicianSelection(event) {
    const value = event.target.value;
    this.setState({ technician: value });
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Select Technician To Remove</h1>
            <form onSubmit={this.handleSubmit} id="create-technician-form">
              <div className="mb-3">
                <select onChange={this.handleTechnicianSelection} value={this.state.technician} required name="technician" id="technician" className="form-select">
                  <option value="">Choose a Technician</option>
                  {this.state.technicians.map(technician => {
                    return (
                      <option key={technician.id} value={technician.employee_number}>{technician.name}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Remove</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default TechnicianDelete;