import React from 'react';
import { Link } from "react-router-dom";

class ServiceList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            appointments: [],
        };
    };

    async finishAppointment(appt_id) {
        const url = `http://localhost:8080/api/appointments/${appt_id}/`;
        const fetchConfig = {
            method: "put",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify({completion: true})
        };
        const response = await fetch(url, fetchConfig); 
        if (response.ok) {
            window.location.reload();
        }
    }
 
    

    async cancelAppointment(appt_id) {
        const url = `http://localhost:8080/api/appointments/${appt_id}/`;
        const fetchConfig = {
            method: "delete"
        };
        const response = await fetch(url, fetchConfig); 
        if (response.ok) {
            window.location.reload();
        }
    }

    async componentDidMount() {
        const url = 'http://localhost:8080/api/appointments/';

        const response = await fetch(url);
        const data = await response.json();

        if(response.ok) {
            this.setState({appointments: data.appointments})
            console.log(data.appointments)
        }
    }

    render() {
        return (
            <table className="table table-striped table-hover">
            <thead>
                <tr>
                <th>VIN</th>
                <th>Customer</th>
                <th>Date and Time</th>
                <th>Reason</th>
                <th>Technician</th>
                <th>VIP</th>
                </tr>
            </thead>
          <tbody>
            {this.state.appointments.map(appointment => {
                let dateTime = Date.parse(appointment.date_time)
                const newDate = new Date(dateTime)
                let vipStatus = 'No';
                let completion = ''
                if (appointment.vip === true) {
                    vipStatus = 'Yes';
                }
                if (appointment.completion === true) {
                    completion = 'd-none'
                }

              return (
                <tr key={appointment.automobile_vin}>
                  <td className={completion}>{ appointment.automobile_vin }</td>
                  <td className={completion}>{ appointment.customer }</td>
                  <td className={completion}>{ newDate.toLocaleString( 'en-US', {month:'long', day:'numeric', year:'numeric', minute:'numeric'})}</td>
                  <td className={completion}>{ appointment.reason }</td>
                  <td className={completion}>{ appointment.technician.name }</td>
                  <td className={completion}>{ vipStatus }</td>
                  <td className={completion}>
                        <button onClick={() => this.cancelAppointment(appointment.id)} type='button'>Cancel</button>
                        <button onClick={() => this.finishAppointment(appointment.id)} type='button'>Finish</button>
                  </td>
                </tr>
              );
            })}
            <tr>
                <td>
                    <Link to='/appointments/new'>Create a new appointment</Link>
                </td>
            </tr>
          </tbody>
        </table>
        )
    }
}

export default ServiceList