import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';



// async function loadManufacturers() {
//   const response = await fetch('http://localhost:8100/api/manufacturers/');
//   if (response.ok) {
//     const data = await response.json();
  
    const root = ReactDOM.createRoot(document.getElementById('root'));
//     root.render(
//       <React.StrictMode>
//         <App manufacturers={data.manufacturers} />
//       </React.StrictMode>
//     );
//   } else {
//     console.error("Error", response)
//   }
// }
// loadManufacturers();
// const root = ReactDOM.createRoot(document.getElementById('root'));

async function loadData() {
  let manufactorsData, vehicleModelsData, automobilesData, salesPersonData, customerData, technicianData;
  const manufactorsResponse = await fetch('http://localhost:8100/api/manufacturers/');
  const vehicleModelsResponse = await fetch('http://localhost:8100/api/models/');
  const automobilesResponse = await fetch('http://localhost:8100/api/automobiles/');
  const salesPersonResponse = await fetch('http://localhost:8090/api/sales_person/');
  const customerResponse = await fetch('http://localhost:8090/api/customers/');
  const technicianResponse = await fetch('http://localhost:8080/api/technicians/');

  if (manufactorsResponse.ok) {
    manufactorsData = await manufactorsResponse.json();
    //console.log('manufactors data: ', manufactorsData)
  } else {
    console.error(manufactorsResponse);
  }
  if (vehicleModelsResponse.ok) {
    vehicleModelsData = await vehicleModelsResponse.json();
    //console.log('vehicleModels data: ', vehicleModelsData)
  } else {
    console.error(vehicleModelsResponse);
  }
  if (automobilesResponse.ok) {
    automobilesData = await automobilesResponse.json();
    //console.log('automobiles data: ', automobilesData)
  } else {
    console.error(automobilesResponse);
  }
  if(salesPersonResponse.ok) {
    salesPersonData = await salesPersonResponse.json();
  } else {
    console.error(salesPersonResponse);
  }
  if(customerResponse.ok) {
    customerData = await customerResponse.json();
  } else {
    console.error(customerResponse);
  }
  if(technicianResponse.ok) {
    technicianData = await technicianResponse.json();
  } else {
    console.error(technicianResponse);
  }


  root.render(
    <React.StrictMode>
      <App manufacturers={manufactorsData.manufacturers} models={vehicleModelsData.models} autos={automobilesData.autos} sales_person={salesPersonData.sales_person} customer={customerData.potential_customer} technician={technicianData.technicians} />
    </React.StrictMode>
  );
};
loadData();
