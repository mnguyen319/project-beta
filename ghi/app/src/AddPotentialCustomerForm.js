import React from 'react';

class AddPotentialCustomerForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email:'',
            phone_number:'',
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePhoneNumberChange = this.handlePhoneNumberChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    };

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        const potentialCustomerUrl = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(potentialCustomerUrl, fetchConfig);
        if (response.ok) {
            const newCustomer = await response.json();
            console.log(newCustomer);
            const cleared = {
                name: '',
                email:'',
                phone_number:'',
              };
              this.setState(cleared);
        }
    };

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value});
    };

    handleEmailChange(event) {
        const value = event.target.value;
        this.setState({email: value});
    };

    handlePhoneNumberChange(event) {
        const value = event.target.value;
        this.setState({phone_number: value});
    };



    render() {
        return(
            <div className="my-5 container">
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1>Potential customer</h1>
                            <form onSubmit={this.handleSubmit} id="create-model-form">
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text"  name="name" id="name" className="form-control"/>
                                    <label htmlFor="name">Name</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleEmailChange} value={this.state.email} placeholder="Email" required type="text" name="email" id="email" className="form-control"/>
                                    <label htmlFor="email">Email</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={this.handlePhoneNumberChange} value={this.state.phone_number} placeholder="Phone Number" required type="text" name="phone_number" id="phone_number" className="form-control"/>
                                    <label htmlFor="phone_number">Phone Number</label>
                                </div>
                                <button className="btn btn-primary">Add</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    };

};

export default AddPotentialCustomerForm;