from django.urls import path

from .views import (
    api_technicians,
    api_technician,
    api_service_appts,
    api_service_appt,
)

urlpatterns = [
    path("technicians/", api_technicians, name="create_technician"),
    path("technicians/<int:pk>/", api_technician, name="show_technician"),
    path("appointments/", api_service_appts, name="list_appointments"),
    path("appointments/<int:pk>/", api_service_appt, name="update_appointment")

]